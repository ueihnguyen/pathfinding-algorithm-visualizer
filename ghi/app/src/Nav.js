function Nav() {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark fs-5"
      style={{ backgroundColor: "#2f404d" }}
    >
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
          Made by tonyngth
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://gitlab.com/tonyngth/pathfinding-algorithm-visualizer"
                target="_blank"
                rel="noreferrer"
              >
                Source Code
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                href="https://tonyngcodes.com/"
                target="_blank"
                rel="noreferrer"
              >
                About me
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;

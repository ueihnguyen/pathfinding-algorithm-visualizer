import React from "react";

function Node(props) {
  const {
    row,
    col,
    isStart,
    isEnd,
    isWall,
    onMouseDown,
    onMouseUp,
    onMouseEnter,
  } = props;

  function getClassName() {
    if (isStart) return "node node-start";
    else if (isEnd) return "node node-end";
    else if (isWall) return "node node-wall";
    else return "node";
  }
  return (
    <td
      id={`node-${row}-${col}`}
      className={getClassName()}
      onMouseDown={() => onMouseDown(row, col)}
      onMouseUp={() => onMouseUp(row, col)}
      onMouseEnter={() => onMouseEnter(row, col)}
    ></td>
  );
}

export default Node;

import { useEffect } from "react";
import useState from "react-usestateref";
import "./App.css";
import Node from "./Node";
import Nav from "./Nav";

function App() {
  const totalCols = 50;
  const totalRows = 25;
  const [startRow, setStartRow] = useState(7);
  const [startCol, setStartCol] = useState(11);
  const [endRow, setEndRow] = useState(7);
  const [endCol, setEndCol] = useState(38);
  const [board, setBoard] = useState([]);
  const [isRunning, setIsRunning] = useState(false);
  const [selectedRow, setSelectedRow, selectedRowRef] = useState(0);
  const [selectedCol, setSelectedCol, selectedColRef] = useState(0);
  const [selectedStart, setSelectedStart] = useState(false);
  const [selectedEnd, setSelectedEnd] = useState(false);
  const [selectedWall, setSelectedWall] = useState(false);
  const [isMouseDrag, setIsMouseDrag] = useState(false);

  //----------------------MOUSE EVENT---------------------------
  function handleMouseDown(row, col) {
    if (!isRunning) {
      if (isClear()) {
        const nodeClassName = document.getElementById(
          `node-${row}-${col}`
        ).className;

        if (nodeClassName === "node node-start") {
          setIsMouseDrag(true);
          setSelectedStart(true);
          setSelectedRow(row);
          setSelectedCol(col);
        } else if (nodeClassName === "node node-end") {
          setIsMouseDrag(true);
          setSelectedEnd(true);
          setSelectedRow(row);
          setSelectedCol(col);
        } else {
          setIsMouseDrag(true);
          setSelectedWall(true);
          setSelectedRow(row);
          setSelectedCol(col);
          document.getElementById(`node-${row}-${col}`).className =
            "node node-wall";
          board[row][col].isWall = !board[row][col].isWall;
        }
      } else {
        handleResetBoard();
      }
    }
  }

  function handleMouseUp(row, col) {
    if (!isRunning) {
      setIsMouseDrag(false);
      if (selectedStart) {
        setStartCol(col);
        setStartRow(row);
        setSelectedStart((value) => !value);
      } else if (selectedEnd) {
        setEndCol(col);
        setEndRow(row);
        setSelectedEnd((value) => !value);
      }
    }
  }

  //here I implemented 'react-usestateref' npm package since
  //  using regular useState from 'react' will sometimes lead to
  //  "react batching states updates" that will cause setState method
  //  not reflecting change immediately. Therefore, it can cause
  //  unwanting effects when we move the mouse too fast when dragging
  //  nodes around the board

  function handleMouseEnter(row, col) {
    if (!isRunning && isMouseDrag) {
      const nodeClassName = document.getElementById(
        `node-${row}-${col}`
      ).className;

      // if a start node was selected
      if (selectedStart && nodeClassName !== "node node-wall") {
        //make the selected start node into a regular node
        board[selectedRowRef.current][selectedColRef.current].isStart = false;
        document.getElementById(
          `node-${selectedRowRef.current}-${selectedColRef.current}`
        ).className = "node";
        //update the selected row and col into the current
        //node where the mouse just entered
        setSelectedRow(row);
        setSelectedCol(col);
        //make the new selected node into a start node
        board[row][col].isStart = true;
        document.getElementById(`node-${row}-${col}`).className =
          "node node-start";
        setStartRow(row);
        setStartCol(col);
      } else if (selectedEnd && nodeClassName !== "node node-wall") {
        board[selectedRowRef.current][selectedColRef.current].isEnd = false;
        document.getElementById(
          `node-${selectedRowRef.current}-${selectedColRef.current}`
        ).className = "node";
        setSelectedRow(row);
        setSelectedCol(col);
        board[row][col].isEnd = true;
        document.getElementById(`node-${row}-${col}`).className =
          "node node-end";
        setEndRow(row);
        setEndCol(col);
      } else if (selectedWall) {
        if (nodeClassName === "node") {
          document.getElementById(`node-${row}-${col}`).className =
            "node node-wall";
        } else if (nodeClassName === "node node-wall") {
          document.getElementById(`node-${row}-${col}`).className = "node";
        }
        board[row][col].isWall = !board[row][col].isWall;
        console.log(row, col, board[row][col].isWall);
      }
    }
  }

  function handleMouseLeave() {
    if (selectedStart) {
      setSelectedStart((value) => !value);
      setIsMouseDrag(false);
    } else if (selectedEnd) {
      setSelectedEnd((value) => !value);
      setIsMouseDrag(false);
    } else if (selectedWall) {
      setSelectedWall((value) => !value);
      setIsMouseDrag(false);
    }
  }

  // function to check to see if any node in board haven't been cleared
  function isClear() {
    for (let row of board) {
      for (let node of row) {
        const nodeClassName = document.getElementById(
          `node-${node.row}-${node.col}`
        ).className;
        if (
          nodeClassName === "node node-visited" ||
          nodeClassName === "node node-shortest-path"
        ) {
          return false;
        }
      }
    }
    return true;
  }

  // function to set the board between is running or not running
  function toggleIsRunning() {
    setIsRunning((value) => !value);
  }

  // set every node in the board back to default, except start, end and wall Node
  const handleResetBoard = () => {
    if (!isRunning) {
      console.log("Clear");
      for (let row of board) {
        for (let node of row) {
          const nodeClassName = document.getElementById(
            `node-${node.row}-${node.col}`
          ).className;
          if (
            nodeClassName !== "node node-start" &&
            nodeClassName !== "node node-end" &&
            nodeClassName !== "node node-wall"
          ) {
            document.getElementById(`node-${node.row}-${node.col}`).className =
              "node";
            node.isVisited = false;
            node.f = Infinity;
            node.h = heuristic(node.row, node.col, endRow, endCol);
          }
          if (nodeClassName === "node node-start") {
            node.isVisited = false;
            node.f = Infinity;
            node.h = heuristic(node.row, node.col, endRow, endCol);
            node.isStart = true;
            node.isWall = false;
            node.previousNode = null;
          }
          if (nodeClassName === "node node-end") {
            node.isVisited = false;
            node.f = Infinity;
            node.h = 0;
            node.isEnd = true;
            node.isWall = false;
            node.previousNode = null;
          }
        }
      }
    }
  };

  // set every wall node in board to default, except start and end node
  function handleResetWall() {
    for (let row of board) {
      for (let node of row) {
        const nodeClassName = document.getElementById(
          `node-${node.row}-${node.col}`
        ).className;
        if (nodeClassName === "node node-wall") {
          document.getElementById(`node-${node.row}-${node.col}`).className =
            "node";
          node.isWall = false;
          node.isVisited = false;
          node.f = Infinity;
          node.h = heuristic(node.row, node.col, endRow, endCol);
          node.previousNode = null;
        }
      }
    }
  }

  // run the pathfinding animation and visualize the shortest path
  const handleVisualize = () => {
    if (!isRunning) {
      handleResetBoard();
      toggleIsRunning();
      const startNode = board[startRow][startCol];
      const endNode = board[endRow][endCol];
      startNode.f = 0;
      const visitedNodes = [];
      const unvisitedNodes = getUnvisitedNodes();

      while (unvisitedNodes.length > 0) {
        unvisitedNodes.sort(sortByFthenH);
        let nextNode = unvisitedNodes.shift();
        if (!nextNode.isWall) {
          if (nextNode.f === Infinity) break;
          nextNode.isVisited = true;
          visitedNodes.push(nextNode);
          if (nextNode === endNode) break;
          calculateNeighbors(nextNode, board);
        }
      }

      const shortestPath = getShortestPath(endNode);
      visualize(visitedNodes, shortestPath);
    }
  };

  //Visualize visited nodes and the shortest path by changing the
  //  className of each node in the table
  function visualize(visitedNodes, shortestPath) {
    visitedNodes.forEach((node, idx) => {
      //Visualize shortest path
      if (idx === visitedNodes.length - 1) {
        setTimeout(() => {
          shortestPath.forEach((node, idx) => {
            setTimeout(() => {
              if (node === "finish") {
                setTimeout(() => {
                  toggleIsRunning();
                }, idx * 10);
              } else {
                const nodeClassName = document.getElementById(
                  `node-${node.row}-${node.col}`
                ).className;
                if (
                  nodeClassName !== "node node-start" &&
                  nodeClassName !== "node node-end"
                )
                  document.getElementById(
                    `node-${node.row}-${node.col}`
                  ).className = "node node-shortest-path";
              }
            }, 70 * idx);
          });
        }, 10 * (idx + 1));
      }
      //Visualize visited nodes
      setTimeout(() => {
        const nodeClassName = document.getElementById(
          `node-${node.row}-${node.col}`
        ).className;
        if (
          nodeClassName !== "node node-start" &&
          nodeClassName !== "node node-end"
        )
          document.getElementById(`node-${node.row}-${node.col}`).className =
            "node node-visited";
      }, 10 * idx);
    });
  }

  //Find the shortest path by traversing from the endNode to startNode
  function getShortestPath(endNode) {
    const shortestPath = [];
    let currNode = endNode;
    while (currNode !== null) {
      shortestPath.unshift(currNode);
      currNode = currNode.previousNode;
    }
    shortestPath.push("finish");
    return shortestPath;
  }

  //get all unvisited nodes on the board, including start and end nodes
  function getUnvisitedNodes() {
    let list = [];
    for (let row of board) {
      for (let node of row) {
        list.push(node);
      }
    }
    return list;
  }

  //sort function to always keep unvisitedNodes sorting from lowest F value
  //Node to highest F value Node. If F value is equal, use H value to sort
  function sortByFthenH(node1, node2) {
    if (node1.f < node2.f) return -1;
    else if (node1.f > node2.f) return 1;
    else {
      if (node1.h < node2.h) return -1;
      else if (node1.h < node2.h) return 1;
      else return 0;
    }
  }

  //update F value for neighbors of nextNode
  function calculateNeighbors(node, board) {
    const neighbors = getUnvisitedNeighbors(node, board);
    for (let neighbor of neighbors) {
      // neighbor.f = node.f + 10 + node.h;
      // neighbor.previousNode = node;
      neighbor.f = node.f + 10 + node.h;
      neighbor.previousNode = node;
    }
  }

  //get neighbors of nextNode that are not visited
  function getUnvisitedNeighbors(node, board) {
    let neighbors = [];
    if (node.row > 0) neighbors.push(board[node.row - 1][node.col]); //top cell
    if (node.row < board.length - 1)
      neighbors.push(board[node.row + 1][node.col]); //bottom cell
    if (node.col > 0) neighbors.push(board[node.row][node.col - 1]); //left cell
    if (node.col < board[0].length - 1)
      neighbors.push(board[node.row][node.col + 1]); //right cell
    return neighbors.filter((neighbor) => !neighbor.isVisited);
  }

  //load the board when page 1st render
  useEffect(() => {
    setUpBoard();
  }, []);

  //function to set up board the very first time
  function setUpBoard() {
    let board = [];
    for (let i = 0; i < totalRows; i++) {
      let row = [];
      for (let j = 0; j < totalCols; j++) {
        row.push(newNode(i, j));
      }
      board.push(row);
    }
    setBoard(board);
  }

  //function to make every item in the board is a Node
  function newNode(row, col) {
    let node = {
      row: row,
      col: col,
      isStart: row === startRow && col === startCol,
      isEnd: row === endRow && col === endCol,
      isWall: false,
      isVisited: false,
      previousNode: null,
      h: heuristic(row, col, endRow, endCol),
      f: Infinity,
    };
    return node;
  }

  //function to calculate distance from a Node to the endNode
  function heuristic(row, col, endRow, endCol) {
    let h = Math.round(
      Math.sqrt(Math.abs(row - endRow) ** 2 + Math.abs(col - endCol) ** 2) * 10
    );
    return h;
  }

  if (board.length > 0) {
    return (
      <>
        <Nav />
        <div className="container">
          <div className="text-center mt-5">
            <h1>
              <b>Pathfinding Algorithm Visualizer</b>
            </h1>
            <div className="mt-2 text-center font-monospace text-muted">
              <p style={{ fontSize: "17px" }}>Made by tonyngth 2022</p>
            </div>
          </div>
          <div className="text-center">
            <button
              type="button"
              className="btn btn-warning ms-2"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              Tutorial
            </button>
            <button
              type="button"
              className="btn btn-primary ms-2"
              onClick={handleVisualize}
            >
              Visualize A*
            </button>
            <button
              type="button"
              className="btn btn-danger ms-2"
              onClick={handleResetBoard}
            >
              Reset Board
            </button>
            <button
              type="button"
              className="btn btn-dark ms-2"
              onClick={handleResetWall}
            >
              Reset Wall
            </button>
            <table
              className="grid-container mt-5"
              onMouseLeave={() => handleMouseLeave()}
            >
              <tbody className="grid">
                {board.map((row, rowIdx) => {
                  return (
                    <tr key={rowIdx}>
                      {row.map((node, nodeIdx) => {
                        const { row, col, isEnd, isStart, isWall } = node;
                        return (
                          <Node
                            key={nodeIdx}
                            row={row}
                            col={col}
                            isStart={isStart}
                            isEnd={isEnd}
                            isWall={isWall}
                            onMouseDown={handleMouseDown}
                            onMouseUp={handleMouseUp}
                            onMouseEnter={handleMouseEnter}
                          />
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  } else {
    return <h1>is loading...</h1>;
  }
}

export default App;
